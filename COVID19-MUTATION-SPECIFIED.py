from Bio import SeqIO
import pandas as pd

path="/Users/scott/Downloads"

    keepList = ['Q314K','L452M','Y453F','F486L', 'V367F', 'N501T']
    minkFile="spikeprot1205_ALIGN_2_MINK.fasta"
    humanFile = "spikeprot1205_ALIGN_2_HUMAN.fasta"
    protSeq="MFVFLVLLPLVSSQCVNLTTRTQLPPAYTNSFTRGVYYP" \
            "DKVFRSSVLHSTQDLFLPFFSNVTWFHAIHVSGTNGTKR" \
            "FDNPVLPFNDGVYFASTEKSNIIRGWIFGTTLDSKTQSL" \
            "LIVNNATNVVIKVCEFQFCNDPFLGVYYHKNNKSWMESE" \
            "FRVYSSANNCTFEYVSQPFLMDLEGKQGNFKNLREFVFK" \
            "NIDGYFKIYSKHTPINLVRDLPQGFSALEPLVDLPIGIN" \
            "ITRFQTLLALHRSYLTPGDSSSGWTAGAAAYYVGYLQPR" \
            "TFLLKYNENGTITDAVDCALDPLSETKCTLKSFTVEKGI" \
            "YQTSNFRVQPTESIVRFPNITNLCPFGEVFNATRFASVY" \
            "AWNRKRISNCVADYSVLYNSASFSTFKCYGVSPTKLNDL" \
            "CFTNVYADSFVIRGDEVRQIAPGQTGKIADYNYKLPDDF" \
            "TGCVIAWNSNNLDSKVGGNYNYLYRLFRKSNLKPFERDI" \
            "STEIYQAGSTPCNGVEGFNCYFPLQSYGFQPTNGVGYQP" \
            "YRVVVLSFELLHAPATVCGPKKSTNLVKNKCVNFNFNGL" \
            "TGTGVLTESNKKFLPFQQFGRDIADTTDAVRDPQTLEIL" \
            "DITPCSFGGVSVITPGTNTSNQVAVLYQDVNCTEVPVAI" \
            "HADQLTPTWRVYSTGSNVFQTRAGCLIGAEHVNNSYECD" \
            "IPIGAGICASYQTQTNSPRRARSVASQSIIAYTMSLGAE" \
            "NSVAYSNNSIAIPTNFTISVTTEILPVSMTKTSVDCTMY" \
            "ICGDSTECSNLLLQYGSFCTQLNRALTGIAVEQDKNTQE" \
            "VFAQVKQIYKTPPIKDFGGFNFSQILPDPSKPSKRSFIE" \
            "DLLFNKVTLADAGFIKQYGDCLGDIAARDLICAQKFNGL" \
            "TVLPPLLTDEMIAQYTSALLAGTITSGWTFGAGAALQIP" \
            "FAMQMAYRFNGIGVTQNVLYENQKLIANQFNSAIGKIQD" \
            "SLSSTASALGKLQDVVNQNAQALNTLVKQLSSNFGAISS" \
            "VLNDILSRLDKVEAEVQIDRLITGRLQSLQTYVTQQLIR" \
            "AAEIRASANLAATKMSECVLGQSKRVDFCGKGYHLMSFP" \
            "QSAPHGVVFLHVTYVPAQEKNFTTAPAICHDGKAHFPRE" \
            "GVFVSNGTHWFVTQRNFYEPQIITTDNTFVSGNCDVVIG" \
            "IVNNTVYDPLQPELDSFKEELDKYFKNHTSPDVDLGDIS" \
            "GINASVVNIQKEIDRLNEVAKNLNESLIDLQELGKYEQY" \
            "IKWPWYIWLGFIAGLIAIVMVTIMLCCMTSCCSCLKGCC" \
            "SCGSCCKFDEDDSEPVLKGVKLHYT"

with open(path+"/"+minkFile, "r") as fasta_file:
    seqs = []
    protein = []
    species = []
    country = []
    collection_date = []
    id = []
    passage_type = []
    city_state_province = []
    lenSeq = []
    i=0
    gapOrUnknown = []
    mutationsList = []
    mutationsCount = []
    mutations2 = []
    for seq_record in SeqIO.parse(fasta_file, 'fasta'):
        i=i+1
        length = [len(protSeq),len(seq_record.seq)]
        g=0
        m=0
        mutations = []
        for a in range(0,min(length)):
            if seq_record.seq[a] != "X" and seq_record.seq[a] != "-" and seq_record.seq[a] != protSeq[a] and \
                protSeq[a]+str(a+1)+seq_record.seq[a] in keepList:
                    mutations.append(protSeq[a]+str(a+1)+seq_record.seq[a])
                    mutations2.append(protSeq[a] + str(a + 1) + seq_record.seq[a])
                    m=m+1
            if seq_record.seq[a] == "X" or seq_record.seq[a] == "-":
                g=g+1
        if mutations:
            mutationsList.append(mutations)
            mutationsCount.append(m)
            lenSeq.append(min(length))
            gapOrUnknown.append(g)
            protein.append(seq_record.id.split("|")[0])
            species.append(seq_record.id.split("/")[1])
            country.append(seq_record.id.split("/")[2])
            collection_date.append(seq_record.id.split("|")[2])
            id.append(seq_record.id.split("|")[3])
            passage_type.append(seq_record.id.split("|")[4])
            city_state_province.append(seq_record.id.split("|")[5].split("^^")[1])
    mink_df = pd.DataFrame({"PROTEIN": protein, "SPECIES": species, "COUNTRY":country,
                            "COLLECTION_DATE": collection_date, "ID": id,
                            "PASSAGE_TYPE": passage_type, "CITY_STATE_PROVINCE": city_state_province,
                            "LENGTH": lenSeq, "GAP_OR_UNKNOWN": gapOrUnknown, "MUTATIONS": mutationsList,
                            "MUTATIONS_COUNT": mutationsCount,"TOTAL": i, "MUTATED": len(mutationsList)})
    mink_df=mink_df.sort_values(by="COLLECTION_DATE")
    mutations2=set(mutations2)



with open(path+"/"+humanFile, "r") as fasta_file:
    seqs = []
    protein = []
    species = []
    country = []
    collection_date = []
    id = []
    passage_type = []
    city_state_province = []
    lenSeq = []
    i=0
    gapOrUnknown = []
    mutationsList = []
    mutationsCount = []
    mutationsMatch = []
    for seq_record in SeqIO.parse(fasta_file, 'fasta'):
        i=i+1
        length = [len(protSeq),len(seq_record.seq)]
        g=0
        m=0
        mm=0
        mutations = []
        for a in range(0,min(length)):
            if seq_record.seq[a] != "X" and seq_record.seq[a] != "-" and seq_record.seq[a] != protSeq[a]:
                m=m+1
                if protSeq[a] + str(a + 1) + seq_record.seq[a] in mutations2:
                    mutations.append(protSeq[a] + str(a + 1) + seq_record.seq[a])
                    mm=mm+1
            if seq_record.seq[a] == "X" or seq_record.seq[a] == "-":
                g=g+1
        if mutations:
            mutationsMatch.append(mm)
            mutationsList.append(mutations)
            mutationsCount.append(m)
            lenSeq.append(min(length))
            gapOrUnknown.append(g)
            protein.append(seq_record.id.split("|")[0])
            species.append(seq_record.id.split("|")[6])
            country.append(seq_record.id.split("/")[1])
            collection_date.append(seq_record.id.split("|")[2])
            id.append(seq_record.id.split("|")[3])
            passage_type.append(seq_record.id.split("|")[4])
            city_state_province.append(seq_record.id.split("|")[5].split("^^")[1])
    human_df = pd.DataFrame({"PROTEIN": protein, "SPECIES": species, "COUNTRY":country,
                            "COLLECTION_DATE": collection_date, "ID": id,
                            "PASSAGE_TYPE": passage_type, "CITY_STATE_PROVINCE": city_state_province,
                            "LENGTH": lenSeq, "GAP_OR_UNKNOWN": gapOrUnknown, "MUTATIONS": mutationsList,
                            "MUTATIONS_MATCH": mutationsMatch, "MUTATIONS_COUNT": mutationsCount,"TOTAL": i,
                             "MUTATED": len(mutationsList)})
    human_df=human_df.sort_values(by="COLLECTION_DATE")


import numpy as np
mink_df_counts = pd.DataFrame({"counts": list(np.concatenate(mink_df["MUTATIONS"].to_list()))})
human_df_counts = pd.DataFrame({"counts": list(np.concatenate(human_df["MUTATIONS"].to_list()))})
print(mink_df_counts["counts"].value_counts())
print(human_df_counts["counts"].value_counts())


human_df.to_csv(path+"/SELECTED_"+humanFile, index=False)
mink_df.to_csv(path+"/SELECTED_"+minkFile, index=False)

# count_human=(human_df_counts["counts"].value_counts())
# count_human.to_csv(path+"/COUNTS_T"+humanFile, index=True)
#
# count_mink=(mink_df_counts["counts"].value_counts())
# count_mink.to_csv(path+"/COUNTS_"+minkFile, index=True)


